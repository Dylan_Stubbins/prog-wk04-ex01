﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk04_ex01
{
    class Program
    {
        static void Main(string[] args)
        {
            var border = "**********";
            var space = "     ";

            Console.WriteLine($"{border}{border}{border}{border}");
            Console.WriteLine($"{border}{space}{space}{space}{space}{border}");
            Console.WriteLine($"{border}{space}01 Option1{space}{border}");
            Console.WriteLine($"{border}{space}02 Option2{space}{border}");
            Console.WriteLine($"{border}{space}03 Option3{space}{border}");
            Console.WriteLine($"{border}{space}{space}{space}{space}{border}");
            Console.WriteLine($"{border}{border}{border}{border}");

            Console.ReadLine();
        }
    }
}
